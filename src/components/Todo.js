import React, { useState } from 'react'

function Todo() {

    const [newTask, setNewTask] = useState("");
    const [tasks, setTasks] = useState([
        {text: "Create Todo App", finished: true},
        {text: "To drink a coffee", finished: false}
    ]);

    function handleChange(e){
        setNewTask(e.target.value)
    }

    function handleSubmit(e){
        e.preventDefault()
        if (newTask.length > 0) {
            setTasks(
                [
                    ...tasks,
                    {text: newTask, finished: false}
                ]
            )
            setNewTask("")
        }
    }

    function handleDelete(index) {
        const newTasks = [...tasks];
        newTasks.splice(index, 1)
        setTasks(newTasks)
    }

    function handleCheck(index) {
        const newTasks = [...tasks];
        newTasks[index].finished =!newTasks[index].finished;
        setTasks(newTasks)
    }

    const tasksList = tasks.map((item, index) => <div key={index}> <label className={item.finished ? "finished" : ""}><input type="checkbox" checked={item.finished} onChange={() => handleCheck(index)}/>{item.text}</label><button onClick={() => handleDelete(index)}>x</button></div>)

    return (
        <div>
            <div>
                <form onSubmit={handleSubmit}>
                    <input type="text" value={newTask} className="create-item-todo" onChange={handleChange} /> 
                    <button>
                    +
                    </button>
                </form>
            </div>
            <hr />
            <div className="todo-items-list">
                {tasksList}
            </div>
        </div>
    )
}

export default Todo